
docker-compose := docker-compose -f docker-compose.yml

upd:
	sudo docker network inspect reverse-proxy >/dev/null 2>&1 || \
    sudo docker network create --driver bridge reverse-proxy

	${docker-compose} up -d

down:
	${docker-compose} down

shell:
	${docker-compose} exec reverse_proxy sh
